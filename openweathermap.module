<?php
/**
 * @file
 * Handles importing and saving of weather data as entity.
 */

/**
 * Implements hook_menu().
 */
function openweathermap_menu() {
  $items['admin/config/services/openweathermap'] = array(
    'title' => 'Openweathermap',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('openweathermap_admin_form'),
    'access arguments' => array('administer weather data'),
    'type' => MENU_NORMAL_ITEM,
    'file' => 'openweather.admin.inc',
  );
  $items['ajax/openweathermap/%'] = array(
    'title' => 'Openweathermap lookup',
    'page callback' => 'openweathermap_single_location_ajax',
    'page arguments' => array(2),
    'access arguments' => array(TRUE),
    'type' => MENU_CALLBACK,
  );

  return $items;
}

/**
 * Implements hook_theme().
 */
function openweathermap_theme($existing, $type, $theme, $path) {
  return array(
    'openweathermap-single-location' => array(
      'template' => 'openweathermap-single-location',
      'variables' => array('location' => NULL, 'choices' => NULL),
    ),
  );
}

/**
 * Implements hook_cron().
 */
function openweathermap_cron() {
  $last = variable_get('openweathermap_cron_last_run', 0);

  // Run hourly.
  if (($last + openweathermap_get_cron_interval()) < REQUEST_TIME) {
    openweathermap_update_data();
    watchdog('openweathermap', 'Updated weather data', array(), WATCHDOG_NOTICE);
    variable_set('openweathermap_cron_last_run', REQUEST_TIME);
  }
}

/**
 * Implements hook_block_info().
 */
function openweathermap_block_info() {
  $blocks['openweathermap_single'] = array(
    'info' => t('Openweathermap single location'),
    'cache' => DRUPAL_NO_CACHE,
  );

  return $blocks;
}

/**
 * Implements hook_block_view().
 */
function openweathermap_block_view($delta = '') {
  $block = array();

  switch ($delta) {
    case 'openweathermap_single':
      if (user_access('view weather data')) {
        $block['subject'] = t('Weather');
        $block['content'] = openweathermap_single_location('adelaide');
      }
      break;
  }
  return $block;
}

/**
 * Returns themed HTML for a location.
 */
function openweathermap_single_location($location = FALSE) {
  drupal_add_js(drupal_get_path('module', 'openweathermap') . '/openweathermap.js');
  drupal_add_js(array('openweathermap' => array('default_location' => openweathermap_get_default_location())), 'setting');

  if (empty($location)) {
    $location = openweathermap_get_all_weather_for_location('adelaide');
  }
  else {
    if (!is_object($location)) {
      // If a string has been passed, make it an object.
      $location = openweathermap_get_all_weather_for_location($location);
    }
  }

  $choices = openweathermap_get_locations();

  return theme('openweathermap-single-location', array('location' => $location, 'choices' => $choices));
}

/**
 * Ajax callback for location changing.
 */
function openweathermap_single_location_ajax($location) {
  print openweathermap_single_location($location);
  drupal_exit();
}

/**
 * Implements hook_permission().
 */
function openweathermap_permission() {
  return array(
    'administer weather data' => array(
      'title' => t('Administer weather data'),
    ),
    'view weather data' => array(
      'title' => t('View weather data'),
    ),
  );
}

/**
 * Our default location when local storage is not set.
 */
function openweathermap_get_default_location() {
  return variable_get('openweathermap_default_location', FALSE);
}

/**
 * Gets the time in seconds to wait between updates to the data.
 */
function openweathermap_get_cron_interval() {
  return variable_get('openweathermap_cron_interval', 3600);
}

/**
 * Gets the forecast url for the Openweathermap API.
 */
function openweathermap_get_forecast_url() {
  return variable_get('openweathermap_forecast_url', 3600);
}

/**
 * This returns the locations string as saved in admin form.
 */
function openweathermap_get_locations_raw() {
  return variable_get('openweathermap_locations', '');
}

/**
 * Returns openweather api search strings.
 */
function openweathermap_get_locations() {
  $locs = openweathermap_get_locations_raw();

  if (!empty($locs)) {
    $locs = explode(PHP_EOL, trim($locs));
    $locs_array = array();
    foreach ($locs as $loc) {
      $l = explode("|", $loc);
      $locs_array[$l[0]] = $l[1];
    }

    return $locs_array;
  }

  return array();
}

/**
 * Implements hook_entity_info().
 */
function openweathermap_entity_info() {
  $return = array(
    'openweathermap_location' => array(
      'label' => t('Openweather Location'),
      'controller class' => 'OpenWeatherLocationController',
      'base table' => 'openweathermap_location',
      'uri callback' => 'entity_class_uri',
      'fieldable' => TRUE,
      'entity keys' => array(
        'id' => 'location_id',
      ),
      'bundle keys' => array(
        'bundle' => 'type',
      ),
      'bundles' => array(),
      'view modes' => array(
        'full' => array(
          'label' => t('Default'),
          'custom settings' => FALSE,
        ),
      ),
    ),
    // 'views controller class' => 'EntityDefaultViewsController',
  );
  return $return;
}

/**
 * The main callback for updating the data.
 */
function openweathermap_update_data() {
  $locations = openweathermap_get_locations();

  foreach ($locations as $label => $search) {
    // Do we already have a location?
    $location_id = openweathermap_get_location_id_from_label($label);
    if (!empty($location_id)) {
      $e = openweathermap_load($location_id);
    }
    else {
      $e = entity_create('openweathermap_location', array());
      $e->label = $label;
      $e->search_string = $search;
    }

    // Get the latest data for the location.
    $e->current_weather = openweathermap_fetch_current_weather($search);
    $e->forecast = openweathermap_fetch_forecast($search);
    entity_save('openweathermap_location', $e);
  }

  return TRUE;
}

/**
 * Request wrapper.
 */
function openweathermap_make_request($url) {
  try {
    $response = drupal_http_request($url);
  }
  catch (Exception $e) {
    throw new Exception('Unable to make http request.');
  }

  if ($response->code <> 200) {
    throw new Exception('HTTP request response status was not ok.');
  }

  $data = json_decode($response->data);

  // Different API's return different counts.
  $rows = 0;
  if (!empty($data->count)) {
    $rows = $data->count;
  }
  elseif (!empty($data->cnt)) {
    $rows = $data->cnt;
  }

  if ($rows == 0) {
    throw new Exception('Search returned zero results.');
  }

  return $data;
}

/**
 * Get the current weather, ie todays, from the API.
 */
function openweathermap_fetch_current_weather($search) {
  $url = openweathermap_get_current_weather_lookup_url();
  $search = str_replace(' ', '+', $search);
  $url = str_replace('%string%', $search, $url);

  try {
    $data = openweathermap_make_request($url);
  }
  catch (Exception $e) {
    throw new Exception('Unable to fetch current weather. Error was: ' . $e->getMessage());
  }

  if (!empty($data)) {
    return $data->list[0];
  }

  return array();
}

/**
 * Fetch the 7 day forecast from the API.
 */
function openweathermap_fetch_forecast($search) {
  $url = openweathermap_get_forecast_lookup_url();
  $search = str_replace(' ', '+', $search);
  $url = str_replace('%string%', $search, $url);

  try {
    $data = openweathermap_make_request($url);
  }
  catch (Exception $e) {
    throw new Exception('Unable to fetch forecast. Error was: ' . $e->getMessage());
  }

  if (!empty($data->list)) {
    return $data->list;
  }

  return array();
}

/**
 * Get the URL to lookup the current weather.
 */
function openweathermap_get_current_weather_lookup_url() {
  $default = 'http://api.openweathermap.org/data/2.5/find?units=metric&q=%string%';
  return variable_get('openweathermap_current_weather_url', $default);
}

/**
 * Get the URL to lookup the weather forecast.
 */
function openweathermap_get_forecast_lookup_url() {
  $default = 'http://api.openweathermap.org/data/2.5/forecast/daily?units=metric&cnt=7&q=%string%';
  return variable_get('openweathermap_forecast_url', $default);
}

/**
 * Gets the unique location_id from a location label.
 */
function openweathermap_get_location_id_from_label($label) {
  $q = db_select('openweathermap_location', 'f')
       ->fields('f', array('location_id'))
       ->condition('label', $label, '=');
  $r = $q->execute();

  $result = $r->fetchAssoc();
  if (empty($result)) {
    return FALSE;
  }
  return end($result);
}

/**
 * Gets current and forecast weather for a location.
 */
function openweathermap_get_all_weather_for_location($label) {
  return openweathermap_load(openweathermap_get_location_id_from_label($label));
}

/**
 * Implements hook_load().
 */
function openweathermap_load($location_id, $reset = FALSE) {
  $locations = openweathermap_load_multiple(array($location_id), array(), $reset);
  return reset($locations);
}

/**
 * Load multiple locations.
 */
function openweathermap_load_multiple($location_ids = array(), $conditions = array(), $reset = FALSE) {
  return entity_load('openweathermap_location', $location_ids, $conditions, $reset);
}

/**
 * Class for handling the location entity.
 */
class OpenWeatherLocation extends Entity {

}

/**
 * Location controller class.
 */
class OpenWeatherLocationController extends EntityAPIController {
  /**
   * Override the save method.
   */
  public function save($entity, DatabaseTransaction $transaction = NULL) {
    $entity->updated = REQUEST_TIME;
    return parent::save($entity, $transaction);
  }
}
