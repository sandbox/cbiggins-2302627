<div class="openweathermap-block">
  <div class="openweathermap-block-title">
    <div class="openweathermap-block-title-location">
      <?php print ucfirst($location->label); ?>
    </div>
    <div class="openweathermap-block-title-change" data-toggle="modal" data-target="#locationChooser">
      Change city
    </div>
  <div class="openweathermap-block-wrapper">

    <div class="openweathermap-block-current">
      <div class="openweathermap-block-current-icon">
        Icon
      </div>
      <div class="openweathermap-block-current-temp">
        Now <?php print $location->current_weather->main->temp; ?>&deg;C
      </div>
      <div class="openweathermap-block-current-temp-range">
        <?php print $location->current_weather->main->temp_min; ?>&deg;C-<?php print $location->current_weather->main->temp_max; ?>&deg;C<br />
        <div class="openweathermap-block-current-day">Today</div>
        <div class="openweathermap-block-current-desc"><?php print $location->current_weather->weather[0]->description; ?></div>
      </div>
    </div>

    <div class="openweathermap-block-forecast">
      <div class="openweathermap-block-forecast-icon">
        Icon
      </div>
      <div class="openweathermap-block-forecast-temp-range">
        <?php print $location->forecast[0]->temp->min; ?>&deg;C-<?php print $location->forecast[0]->temp->max; ?>&deg;C<br />
        <div class="openweathermap-block-forecast-day">Tomorrow</div>
        <div class="openweathermap-block-forecast-desc"><?php print $location->forecast[0]->weather[0]->description; ?></div>
      </div>
    </div>

  </div>
</div>

<!-- Modal - uses Bootstrap -->
<div class="modal fade" id="locationChooser" tabindex="-1" role="dialog" aria-labelledby="locationChooserLabel" aria-hidden="true">

  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">
          <span aria-hidden="true">&times;</span>
          <span class="sr-only">Close</span></button>
        <h4 class="modal-title" id="locationChooserLabel">Choose location</h4>
      </div>
      <div class="modal-body">
        <div class="openweathermap-location-chooser">
          <?php foreach ($choices as $label => $search): ?>
          <?php $l = str_replace(' ', '+', $label); ?>
            <a href="javascript:void(0);" id="<?php print $l; ?>" class="label label-default" data-dismiss="modal"><?php print ucfirst($label); ?></a></br />
          <?php endforeach; ?>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Save changes</button>
      </div>
    </div>
  </div>
</div>
