<?php
/**
 * @file
 * Handles admin functionality for the module
 */

/**
 * Simple admin form for adding/removing weather locations.
 */
function openweathermap_admin_form($form, &$form_state) {
  $form['openweathermap_locations'] = array(
    '#title' => t('Location'),
    '#type' => 'textarea',
    '#description' => t('Pipe separated list of location labels and search strings.'),
    '#default_value' => openweathermap_get_locations_raw(),
  );

  $form['openweathermap_default_location'] = array(
    '#title' => t('Default Location'),
    '#type' => 'text',
    '#description' => t('Default location to show visitors who have not set a preference.'),
    '#default_value' => openweathermap_get_default_location(),
  );

  $form['openweathermap_cron_interval'] = array(
    '#title' => t('Cron Interval'),
    '#type' => 'text',
    '#description' => t('How often do you want weather data updated? Time is in seconds. Default is 3600 (1 hour).'),
    '#default_value' => openweathermap_get_cron_interval(),
  );

  $form['openweathermap_forecast_url'] = array(
    '#title' => t('Forecast URL'),
    '#type' => 'text',
    '#description' => t('The URL to the forecast API on Openweathermap.org. %search% is replaced with location search string set above.'),
    '#default_value' => openweathermap_get_forecast_lookup_url(),
  );

  $form['openweathermap_current_weather_url'] = array(
    '#title' => t('Forecast URL'),
    '#type' => 'text',
    '#description' => t('The URL to the current weather API on Openweathermap.org. %search% is replaced with location search string set above.'),
    '#default_value' => openweathermap_get_current_weather_lookup_url(),
  );

  return system_settings_form($form);
}
