(function ($) {
  Drupal.behaviors.openweathermap = {
    attach: function (context, settings) {

      // Update weather data on load.
      openweathermap_update_page_location(openweathermap_update_get_user_location());

      // On click event for changing locations.
      $('.openweather-location-chooser a').live('click', function(e) {
        openweathermap_location = $(e.target).attr('id');
        openweathermap_update_set_user_location(fow_location);
        $(e.target).removeClass('label-default').addClass('label-success');
        $(e.target).siblings('.label-success').removeClass('label-success').addClass('label-default');
        openweathermap_update_page_location(fow_location);
      })

      function openweathermap_update_page_location(fow_location) {
        $('.openweathermap-block').load('/ajax/openweathermap/' + fow_location, function() {
          // Not sure if we'll have a loader icon to remove here.
        });
      }

      function openweathermap_update_set_user_location(fow_location) {
        localStorage.setItem('openweathermap_location', fow_location);
      }

      function openweathermap_update_get_user_location() {
        l = localStorage.getItem('openweathermap_location');
        if (l == null) {
          l = Drupal.settings.openweathermap.default_location;
        }
        return l;
      }

    }
  };
})(jQuery);
