<?php

/**
 * @file
 * Handles our drush commands.
 */

/**
 * Implements hook_drush_command().
 */
function openweathermap_drush_command() {
  $items['openweathermap-update'] = array(
    'description' => dt('Updates the weather data.'),
    'aliases' => array('owu'),
  );
  return $items;
}

function drush_openweathermap_update() {
  if (openweathermap_update_data()) {
    drush_log('Updated latest weather data.', 'success');
  }
  else {
    drush_log('Unable to retrieve latest weather data.', 'error');
  }
}
